const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector("#span-full-name");
const spanFullName2 = document.querySelector("#span-full-name2");


// Alternative ways to retrieve elements
// document.getElementById('txt-first-name');
// document.getElementsByClassName('txt-input');
// document.getElementsByTagName('input');

// two arguments on addEventListener = identify event and a function that the listener will execute once the event is triggered

txtFirstName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirstName.value;
})

txtLastName.addEventListener('keyup', (event) => {
	spanFullName2.innerHTML = txtLastName.value;
})

// event.target contains the element where the event happens
txtFirstName.addEventListener('keyup', (event) => {
	console.log(event.target);
	console.log(event.target.value);
})